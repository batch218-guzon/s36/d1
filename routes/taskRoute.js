// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res) => {
	// invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
									 //returns result from our controller
	taskController.getAllTask().then(result => res.send(result));
})

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
});

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
});

router.put("/updateTask/:id", (req, res) => {
	//req.params.id - will be the basis of what document we will update
	// req.body - the new document/contents
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});






// ACTIVITY


router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(result => res.send(result));
})

router.put("/:id/complete", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;