// This document contains our app feature in displaying and manipulating our database

const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false; //"Error detected"
		}
		else {
			return task;
		}

	})
}

// "taskId" parameter will serve as storage of id in our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		}
		else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false
			}
			else {
				return updateTask;
			}
		})


	})
}




// ACTIVITY

// [GET -Wildcard required]
// 1. Create a controller function for retrieving a specific task.
// 2. Create a route 
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then(result => result)
}

// [PUT - Update]
// 5. Create a controller function for changing the status of a task to "complete".
// 6. Create a route
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		}
		
		result.status = newContent.status;

		return result.save().then((statusUpdate, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			}
			else {
				return statusUpdate;
			}
		})
	})
}

// 9. Create a git repository named S31.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.
